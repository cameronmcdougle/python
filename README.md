# Python

This is the Python tooling I find useful at work.

## Usage
To install this repo in your python project, run the following:
```
python -m pip install git+https://gitlab.com/cameronmcdougle/python.git#egg=cameron-python
```
### Docker Build
```
docker build -t cameron-python . --progress=plain
```
### Docker Run
```
docker run --rm -it --name cameron-python --volume $(pwd)/:/code cameron-python bash
```

FROM python:latest

WORKDIR /code
RUN apt update

RUN python -m pip install --upgrade pip
